function each(elements, cb) {
    if(Array.isArray(elements)){
        for(let index=0;index<elements.length;index++){
            cb(elements[index],index,elements);
        }
    }else{
        return undefined;
    };
    
};

module.exports=each;
    