function find(elements,cb){
    if(Array.isArray(elements)){
        for(let index=0;index<elements.length;index++){
            if(cb(elements[index],index,elements)){
                return elements[index];
            }
        }
        return undefined;
    }else{
        return null;
    }
    
};

module.exports=find;