function flatten(elements){
    if(Array.isArray(elements)){
        let array=[];
        elements.forEach((element) => {
            if(Array.isArray(element)){
                array=array.concat(flatten(element));
            }else{
                array.push(element);
            }
            
        });
        return array;
    }else{
        return undefined;
    }
    
};

module.exports=flatten;