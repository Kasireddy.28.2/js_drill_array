
function map(elements,cb){
    if(Array.isArray(elements)){
        let newArray=[];
        for(let index=0;index<elements.length;index++){
            newArray.push(cb(elements[index],index,elements));
        };
        return newArray;
    }else{
        return undefined;
    }
    
};

module.exports=map;