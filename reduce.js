function reduce(elements,cb,initialValue){
    if(Array.isArray(elements)){
        let acc=initialValue===undefined?elements[0]:initialValue;
        let startIndex=initialValue===undefined?1:0;

        for(let index=startIndex;index<elements.length;index++){
            acc=cb(acc,elements[index],index,elements);
        };
        return acc;
    }else{
        return undefined;
    }
};

module.exports=reduce;