let flatten=require("../flatten");

const nestedArray = [1, [2], [[3]], [[[4]]]];

let flattenArray=flatten(nestedArray);

console.log(flattenArray);